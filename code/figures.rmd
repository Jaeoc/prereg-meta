---
title: "Figures"
author: "Anton Ohlsson Collentine"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, message = FALSE, warning = FALSE, fig.width = 6, fig.asp = 1.3)
```

```{r load packages, data and source helper functions}
if(!require(readr)){install.packages("readr")}
if(!require(metafor)){install.packages("metafor")}
if(!require(dplyr)){install.packages("dplyr")}
if(!require(purrr)){install.packages("purrr")}
if(!require(ggplot2)){install.packages("ggplot2")}
if(!require(cowplot)){install.packages("cowplot")}

library(readr) #To load data
library(dplyr) #For data transformation
library(purrr) #For data iteration
library(metafor) #To run meta-analyses
library(ggplot2) #Plot data
library(cowplot) #Combine several plots into one

dat <- read_csv("../data/collated_summary_data.csv")

source("helper_functions_tables_figures.r") #Load functions to prep data for figures
```

```{r transform_df}
#Transform effect sizes to raw correlations
#'transform_MA' is a sourced function
res <- dat %>% 
  split(.$effect) %>%  #separate by effect, necessary step otherwise the function is applied overall
  map_dfr(transform_MA, .id = "effect") #apply function and transform back from list to dataframe

```

```{r MA_df}
summarizer <- function(x){
  fit <- rma(measure = "ZCOR", ri = x$r, ni = x$n, data = x) #takes raw correlation as input
  data.frame(z = fit$b[[1]], I2 = fit$I2, H2trunc = fit$H2, H2 = fit$QE / (fit$k - 1)) #estimates out
}
#Default method of metafor for calculating H2 is truncated at one. Alternative method for calculating H2 provides information also on excessive homogeneity, i.e, less variability than expected by chance (that is, does not have a lower limit of 1). This method computes H2 as originally specified by Higgins & Thompson instead of using the more general method of metafor (see ?print.rma.uni).

#Higgins, J., & Thompson, S. G. (2002). Quantifying heterogeneity in a meta-analysis. Statistics in medicine, 21(11), 1539-1558.
  
res2 <- res %>% 
  split(.$effect) %>%  #separate by effect, necessary step otherwise the function is applied overall
  map_dfr(summarizer, .id = "effect") %>% 
  mutate(z = abs(z)) #Absolute values since we are interested in effect size rather than direction
```

```{r plot_res}

# summary(lm(I2 ~ z, data = res2)) #b = 69.40, R2 = 0.61

I2 <- ggplot(res2, aes(x = z, y = I2)) +
  geom_point(alpha = 0.5) +
  geom_smooth(method = "lm", se = FALSE, color = "black") +
  xlab("Fisher's z") +
  coord_cartesian(ylim = c(0, 100)) +
  geom_text(x = 0.15, y = 80, label = "b = 69.40, R2 = 0.61", size = 3.5) +
  theme_classic() +
  scale_y_continuous(name = expression(paste(I^2," index", sep = ""))) +
  scale_x_continuous(name = "Effect size (Fisher's z)")


# summary(lm(H2alt ~ z, data = res2)) #b = 3.81, R2 = 0.53

H2 <- ggplot(res2, aes(x = z, y = H2)) + 
  geom_point(alpha = 0.5) +
  geom_smooth(method = "lm", se = FALSE, color = "black") +
  xlab("Fisher's z") +
  geom_text(x = 0.14, y = 5.9, label = "b = 3.81, R2 = 0.53", size = 3.5) +
  theme_classic() +
  scale_y_continuous(name = expression(paste(H^2," index", sep = ""))) +
  scale_x_continuous(name = "Effect size (Fisher's z)")


effect_het <- plot_grid(I2, H2, nrow = 2, labels = c("A", "B"), align = "v") #combine plots
effect_het
# save_plot("../figures/effect-het.png", effect_het,
          # nrow = 2, base_aspect_ratio = 1.3, dpi = 600)

```

```{r robustness, results="hide"}
##Excluding anchoring effects
dropped_anchoring <- res2 %>%
  filter(!grepl("Anchoring", effect))

summary(lm(I2 ~ z, data = dropped_anchoring))
#b = 87.87, R2 = .51

```

```{r Plot_tau_against_I2, fig.asp = 0.618}
dat3 <- readRDS("../data/tau_simulation_results.RDS")
names(dat3) <- sort(unique(dat$effect)) #names were alphabetized upon splitting in simulation so must sort

OR2d <- c('Allowed vs. forbidden', 'Gain vs. loss framing', 'Norm of reciprocity', 'Low vs. high category scales') #effects that were transformed from OR to SMD

#Categorize effects by type of simulation for graphing
effect_type <- distinct(select(dat, effect, effect_type)) %>% 
  mutate(effect_type = ifelse(effect %in% OR2d | effect_type == "Risk difference" | 
                                effect_type == "Raw mean difference", "MD", effect_type),
         effect_type = recode(effect_type, "d" = 'SMD',
                              "r" = "Fisher's z"))



dat3 <- dat3 %>% 
  bind_rows(.id = "effect") %>% #create dataframe with identifier
  left_join(., effect_type) %>% 
  select(I2, tau, effect, effect_type) %>% 
  group_by(tau, effect, effect_type) %>% 
  summarize(I2 = mean(I2)) %>% #Take mean of I2 at each tau-level and for each effect
  ungroup()

#combined line plot gives good overview. Point version of the same is also interesting.
ggplot(dat3, aes(x = tau, y = I2, group = effect, color = effect_type)) +
  geom_line(alpha = 0.6) +
  theme_bw() +
  scale_x_continuous(name = expression(paste("Between studies standard deviation ", tau)),
                     minor_breaks = NULL) +
  scale_y_continuous(name = expression(paste(I^2," index", sep = "")),
                     minor_breaks = NULL) +
  coord_cartesian(ylim = c(0, 100)) +
  scale_color_brewer(name = "Effect type", palette = "Dark2", 
                     breaks = c("Fisher's z", "MD", "SMD")) +
  theme(legend.position = c(0.8, 0.4),
        legend.background = element_rect(linetype = "solid", color = "black"))
  

# ggsave("../figures/tau-I2.png", dpi = 600, height =  4 , width = 6.47)

```


```{r Density_plot, fig.asp= 0.618}
dens <- readRDS("../data/power_simulation_results.RDS")
names(dens) <-  sort(unique(dat$effect))

#Simulated data at various heterogeneity levels, prep for plotting
I2_dist <- dens %>% 
  bind_rows(.id = "effect") %>% 
  rename(Heterogeneity = tau_index) %>% 
  mutate(Heterogeneity = recode(Heterogeneity,
                            '1' = "Zero",
                            '2' = "Small",
                            '3' = "Medium",
                            '4' = "Large"),
         Heterogeneity = as.factor(Heterogeneity))

#Observed I2 estimates, i.e, results from main table, see table.rmd
#Effects estimated with sourced function 'est_heterogeen_smd_raw'
observed <- dat %>% 
  split(.$effect) %>%  
  map_dfr(est_heterogen_smd_raw, .id = "effect") %>% 
  select(effect, I2 = s_I2) %>%
  mutate(Heterogeneity = "Observed")

ggplot(I2_dist, aes(x = I2, group = Heterogeneity, fill = Heterogeneity, linetype = Heterogeneity)) +
  geom_histogram(data = observed, aes(y = ..density.., x = I2), bins = 100, alpha = 1) +
  geom_density(alpha = 0.3) +
  theme_classic() +
  coord_cartesian(xlim = c(0, 100))  +
  scale_fill_brewer(palette = "Dark2",
                    breaks = c("Zero", "Small", "Medium", "Large", "Observed"),
                    labels = c("Zero (0%)", "Small (25%)", "Medium (50%)", "Large (75%)", "Observed")) +
  scale_linetype_manual(values = c( "dotdash", "dotted", "solid", "solid", "dashed"),
                        breaks = c("Zero", "Small", "Medium", "Large", "Observed"), 
                        labels = c("Zero (0%)", "Small (25%)", "Medium (50%)", "Large (75%)", "Observed")) +
  scale_color_brewer(palette = "Dark2", 
                     breaks = c("Zero", "Small", "Medium", "Large", "Observed"),
                     labels = c("Zero (0%)", "Small (25%)", "Medium (50%)", "Large (75%)", "Observed")) +
  theme(legend.position = c(0.85, 0.71),
        legend.background = element_rect(linetype = "solid", color = "black")) +
  scale_y_continuous(name = "Density") +
  scale_x_continuous(name = expression(paste(I^2," index", sep = "")))

# ggsave("../figures/density-I2.png", dpi = 600, height =  4 , width = 6.47)
```

