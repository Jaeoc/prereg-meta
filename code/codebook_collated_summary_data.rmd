---
dataMaid: yes
title: Codebook for collated_summary_data.csv
subtitle: "Limited evidence for widespread heterogeneity in psychology"
output: pdf_document
documentclass: article
header-includes:
  - \newcommand{\fullline}{\noindent\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}}
  - \renewcommand\familydefault{\sfdefault}
---
```{r , echo=FALSE, include=FALSE, warning=FALSE, message=FALSE, error=FALSE}
library(ggplot2)
library(pander)
```
# Data report overview
The dataset examined has the following dimensions:


---------------------------------
Feature                    Result
------------------------ --------
Number of observations        988

Number of variables            18
---------------------------------








































# Codebook summary table

-----------------------------------------------------------------------------------------------------
Label   Variable               Class         # unique  Missing  Description                          
                                               values                                                
------- ---------------------- ----------- ---------- --------- -------------------------------------
        **[rp]**               character           10  0.00 %   Replication Project                  

        **[effect]**           character           37  0.00 %   Studied effect                       

        **[Site]**             character          200  0.00 %   Lab-identifier within each           
                                                                replication project                  

        **[country]**          character           27  0.00 %   Country of participants {ISO         
                                                                Alpha-3 code}                        

        **[in\_lab]**          integer              2  0.00 %   Whether study took place in-lab {1}  
                                                                or not {0} (0 = online)              

        **[Ntotal]**           integer            226  0.00 %   Total study sample size              

        **[B\_or\_W]**         character            2  0.00 %   Between or within groups study       

        **[design]**           character            9  0.00 %   Description of design of research    

        **[or\_stat\_test]**   character           11  4.96 %   Statistical test used, if any        

        **[effect\_type]**     character            4  0.00 %   Reported effect type {d, r, Risk     
                                                                difference, Raw mean difference}     

        **[effect\_size]**     numeric            902  0.00 %   Size of reported effect              

        **[ncontrol]**         integer            150  11.94 %  Sample size control group            

        **[ntreatment]**       integer            153  11.94 %  Sample size treatment group          

        **[outcomes1\_2]**     character           10  0.00 %   Describes content in outcome_1 and   
                                                                outcome_2. For chi-square            
                                                                effects(group1, group2) indicates    
                                                                'treatment' and 'control' groups     

        **[outcome\_c1]**      numeric            666  14.07 %  Control group outcome 1              

        **[outcome\_t1]**      numeric            675  14.07 %  Treatment group outcome 1            

        **[outcome\_c2]**      numeric            652  18.22 %  Control group outcome 2              

        **[outcome\_t2]**      numeric            635  20.55 %  Treatment group outcome 2            
-----------------------------------------------------------------------------------------------------


\fullline



Report generation information:

 *  Created by Anton Olsson Collentine.

 *  Report creation time: on jun 27 2018 11:39:37

 *  Based on the R-package dataMaid v1.1.2 [Pkg: 2018-05-03 from CRAN (R 3.4.4)]

 *  R version 3.4.3 (2017-11-30).